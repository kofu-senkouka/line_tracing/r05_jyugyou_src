#include <LCD_I2C.h>
#include "moter.h"

#define TEST_SW1 (20)//テストSW1はGP20
#define TEST_SW2 (21)//テストSW2はGP21
#define TEST_LED1 (12)//テストLED1はGP12
#define TEST_LED2 (13)//テストLED2はGP13

LCD_I2C lcd(0x27, 16, 2);//LCD関数をインスタンス化

void setup() {
  // put your setup code here, to run once:
  main_wait_ini(50);//main処理の待ち時間50msにセット
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(TEST_SW1, INPUT_PULLUP);//TEST_SW1をプルアップで入力モードに設定
  pinMode(TEST_SW2, INPUT_PULLUP);//TEST_SW2をプルアップで入力モードに設定
  pinMode(TEST_LED1, OUTPUT);//TEST_LED1を出力モードに設定
  pinMode(TEST_LED2, OUTPUT);//TEST_LED2を出力モードに設定
  lcd.begin();//LCD初期化
  lcd.backlight();//バックライトON
  moter_begin();//モータ関連初期化
  sensor_begin();//センサ関連初期化
}

unsigned short tim_cnt=0;//メイン関数内で50ms毎にカウントアップする
bool LED_ONOFF=false;
char lcd_buff1[17];//16文字＋エンドコードで17文字分確保(1行目)
char lcd_buff2[17];//16文字＋エンドコードで17文字分確保(2行目)

void loop() {
  // put your main code here, to run repeatedly:
   static unsigned long main_wait_time;//メイン周期の待った時間（us）
   main_wait_time = main_wait();//メイン周期待ち関数
   /* この関数以降は50ms周期でループする */
   main_50ms();//50ms毎処理を呼び出す
   
   if((tim_cnt % 20) == 0){
    //50ms * 20 = 1000ms = 1s毎の処理
   }
   if((tim_cnt % 2) == 0){
    main_100ms();//100ms毎処理を呼び出す
   }
   tim_cnt++;//カウントアップ tim_cnt=tim_cnt+1と同じ

   lcd.setCursor(0, 0);//カーソル位置を1文字目の1行目にする
   lcd.print(lcd_buff1);//1行目の文字データを表示
   lcd.setCursor(0, 1);//カーソル位置を1文字目の2行目にする
   lcd.print(lcd_buff2);//2行目の文字データを表示
}

void main_50ms(void){ //50ms毎の処理関数
  ushort sen=0;
  //テスト用のプログラム
  if( digitalRead(TEST_SW1) == LOW){//TEST_SW1がON
    digitalWrite(TEST_LED1,HIGH);//TEST_LED1からHIGHを出力=光る
  }
  else{
    digitalWrite(TEST_LED1,LOW);//TEST_LED1からLOWを出力=消える
  }
  if( digitalRead(TEST_SW2) == LOW){//TEST_SW2がON
    digitalWrite(TEST_LED2,HIGH);//TEST_LED2からHIGHを出力=光る
  }
  else{
    digitalWrite(TEST_LED2,LOW);//TEST_LED2からLOWを出力=消える
  }
  snprintf(lcd_buff1,16,"cnt=%6d", tim_cnt);//文字をprintfフォーマットで文字データに変換
  snprintf(lcd_buff2,16,"SW1=%d, SW2=%d",digitalRead(TEST_SW1), digitalRead(TEST_SW2));
  //モータのプログラム
  //moter_pwm_set(R, 512);//右モータ 50%
  //moter_pwm_set(L, 512);//左モータ 50%
  //センサのプログラム
  sen = sen_read();//センサの値を読込
  snprintf(lcd_buff1, 16, "SEN=%2x", sen);
  switch( sen ){
    case 0x06://直進○●●○
      moter_pwm_set(R, ((100*1023)/100));//右モータ 100%
      moter_pwm_set(L, ((100*1023)/100));//左モータ 100%

      moter_mode(R,CCW);//右逆転
      moter_mode(R,CW);//右正転
      moter_brk(R,BRK_ON);//右ブレーキON
      moter_brk(R,BRK_OFF);//右ブレーキOFF
      break;
    case 0x04://左旋回○●○○
      moter_pwm_set(R, ((100*1023)/100));//右モータ 100%
      moter_pwm_set(L, ((80*1023)/100));//左モータ 80%
      break;
    case 0x02://右旋回○○●○
      moter_pwm_set(R, ((80*1023)/100));//右モータ 80%
      moter_pwm_set(L, ((100*1023)/100));//左モータ 100%
      break;
    case 0x01://急な右旋回○○○●

      break;
    default:
      //処理なし
      break;
  }

}

void main_100ms(void){ //100ms毎の処理関数
  //確認用LEDフリッカ処理
  if( LED_ONOFF == true ){
    digitalWrite(LED_BUILTIN, HIGH);
    LED_ONOFF = false;
  }
  else{
    digitalWrite(LED_BUILTIN, LOW);
    LED_ONOFF = true;
  }
}
