#include "sensor.h"

/* センサ2のAD変換処理 */
int sen2_ad(void){
  return (analogRead(SEN2));
}

/* センサ3のAD変換処理 */
int sen3_ad(void){
  return (analogRead(SEN3));
}

/* センサ読取り */
ushort sen_read(void){
  ushort sen1=0, sen2=0, sen3=0, sen4=0, ret=0;
  int sen2ad=0, sen3ad=0;

  sen1 = digitalRead(SEN1);//センサ1の値を読取り 1/0
  sen4 = digitalRead(SEN4);//センサ4の値を読取り 1/0
  sen2ad = sen2_ad();//センサ2をAD変換
  sen3ad = sen3_ad();//センサ3をAD変換
  if( sen2ad >= 300 ){//数値が大きい＝黒
    sen2 = 1;//センサは黒=高い電圧=1
    digitalWrite(SEN2_LED,HIGH);//センサ2のLEDをON
  }
  else{
    digitalWrite(SEN2_LED,LOW);//センサ2のLEDをOFF
  }
  if( sen3ad >= 300 ){//数値が大きい＝黒
    sen3 = 1;//センサは黒=高い電圧=1
    digitalWrite(SEN3_LED,HIGH);//センサ2のLEDをON
  }
  else{
    digitalWrite(SEN3_LED,LOW);//センサ2のLEDをOFF
  }
  ret = (sen1<<3) + (sen2<<2) + (sen3<<1) + sen4;//4bitにまとめる
  return (ret);
}

/* センサ初期化関数 */
void sensor_begin(void){
  pinMode(SEN1, INPUT);
  pinMode(SEN2, INPUT);
  pinMode(SEN3, INPUT);
  pinMode(SEN4, INPUT);
  pinMode(SEN2_LED, OUTPUT);
  pinMode(SEN3_LED, OUTPUT);
}



















